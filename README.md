# shell-sysinfo

Script to check multiple servers for updates and run the update if necessary.

## Usage
1. download the script:
e.g. `git clone https://codeberg.org/Tealk/shell-server-update-check.git`
2. make the file executable
`chmod +x server-update`
3. execute the file
`bash server-update`
4. (optional) add an alias
if you use bash edit `~/.bashrc` and add `alias serverup="path-to/shell-server-update-check/server-update"`
now you can execute it my simply type `serverup` in your terminal

## Extensions/improvements
Create a report [here](https://codeberg.org/Tealk/shell-server-update-check/issues) and surprise me with your ideas.

## Bug Report
Feel free to create a report [here](https://codeberg.org/Tealk/shell-server-update-check/issues)
